#include <iostream>
#include <vector>
#include <list>
#include <fstream>
#include <list>
#include <set>
#include <limits>
#include <algorithm>
#include <iterator>
#include <sstream>
#include <ctime>

#define STREAMMAX std::numeric_limits<std::streamsize>::max()

using namespace std;


class Zadanie
{
public:
    Zadanie(size_t id_, size_t p_, size_t m_);
    Zadanie();
    size_t id,p,r,q,m;
    size_t poprz_tech, poprz_kol, nast_tech, nast_kol, n_poprzednikow;
};

Zadanie::Zadanie()
{
    r=0;
    q=0;
}

Zadanie::Zadanie(size_t id_, size_t p_, size_t m_)
{
    id=id_;
    p=p_;
    m=m_;
    r=0;
    q=0;
    n_poprzednikow=0;
    poprz_kol=SIZE_MAX;
    nast_kol=SIZE_MAX;
    poprz_tech=SIZE_MAX;
    nast_tech=SIZE_MAX;
}

bool operator< (Zadanie &z1, Zadanie &z2)
{
    return z1.p < z2.p;
}

class compare_zadanie {
  public:
    bool operator() (const Zadanie & e1, const Zadanie & e2)
    {
        return e1.p < e2.p; // strict weak ordering required
    }
};

bool operator== (Zadanie &z1, Zadanie &z2)
{
    return z1.id == z2.id;
}

struct less_than_r
{
    inline bool operator() (const Zadanie& str1, const Zadanie& str2)
    {
        return (str1.r < str2.r);
    }
};

struct less_than_q
{
    inline bool operator() (const Zadanie& str1, const Zadanie& str2)
    {
        return (str1.q < str2.q);
    }
};


class Projekt
{
public:
    vector<Zadanie> zadania; // zadania w kolejnosci zgodnej z id
    //vector<Zadanie> zadania_sort;
    vector<size_t> kolejnosc_topologiczna; // kolejnosc topologiczna (w formie id zadan)
    vector<size_t> kolejnosc_topologiczna_odwroconego;
    size_t zadan, maszyn, operacji;

    //public:
    size_t cmax;
    int wczytaj(string nazwa_pliku, string data_id);
    int sortuj_topologicznie(); // wynik w zmiennej kolejnosc_topologiczna
    void odwroc_sortuj_topologicznie();
    void licz_r_q();
    void insa();

    size_t r_p_poprzedniego_tech(size_t id);
    size_t q_p_nastepnego_tech(size_t id);
    size_t r_p_poprzedniego_kol(size_t id);
    size_t q_p_nastepnego_kol(size_t id);
    size_t m_zadania(size_t id);
    size_t p_zadania(size_t id);
    size_t r_p_zadania(size_t id);
    size_t q_p_zadania(size_t id);
};

int Projekt::sortuj_topologicznie()
{
    kolejnosc_topologiczna.clear();
    list<size_t> zadania_lista;
    for(auto & zadanie: zadania)
        zadania_lista.push_back(zadanie.id);

    vector<Zadanie> zadania_kopia (zadania.begin(), zadania.end());

    list<size_t>::iterator biezace=zadania_lista.begin();

    while(zadania_lista.size()>1)
    {
        while(biezace!=zadania_lista.end() && zadania_kopia[*biezace].n_poprzednikow!=0) //szukanie wierzcholka o stopniu wchodzacym 0..
            biezace++;
        if(biezace==zadania_lista.end())
        {
            //cykl w grafie
            return 1;
        }
        kolejnosc_topologiczna.push_back(*biezace); //...dodawanie go do kolejnosci

        //usuwanie krawedzi zwiazanych z tym wierzcholkiem oraz jego samego

        if(zadania_kopia[*biezace].nast_tech!=SIZE_MAX)
        {
            zadania_kopia[zadania_kopia[*biezace].nast_tech].poprz_tech=SIZE_MAX;
            zadania_kopia[zadania_kopia[*biezace].nast_tech].n_poprzednikow--;
        }

        if(zadania_kopia[*biezace].nast_kol!=SIZE_MAX)
        {
            zadania_kopia[zadania_kopia[*biezace].nast_kol].poprz_kol=SIZE_MAX;
            zadania_kopia[zadania_kopia[*biezace].nast_kol].n_poprzednikow--;
        }

        //biezace = zadania_lista.erase(biezace);
        zadania_lista.erase(biezace);
        //if(biezace==zadania_lista.end())
        biezace=zadania_lista.begin();
    }
    kolejnosc_topologiczna.push_back(zadania_lista.front()); //ostatnie zadanie
    return 0;
}


void Projekt::licz_r_q()
{
    for(auto & biezace: kolejnosc_topologiczna)
    {
        size_t tmp_r=0;
        if (zadania[biezace].poprz_tech!=SIZE_MAX)
            tmp_r = r_p_poprzedniego_tech(biezace);
        if (zadania[biezace].poprz_kol!=SIZE_MAX)
            tmp_r = max(tmp_r, r_p_poprzedniego_kol(biezace));
        zadania[biezace].r=tmp_r;
    }
    for(auto biezace=kolejnosc_topologiczna.rbegin(); biezace!=kolejnosc_topologiczna.rend(); biezace++)
    {
        size_t tmp_q=0;
        if (zadania[*biezace].nast_tech!=SIZE_MAX)
            tmp_q=q_p_nastepnego_tech(*biezace);
        if (zadania[*biezace].nast_kol!=SIZE_MAX)
            tmp_q = max(tmp_q, q_p_nastepnego_kol(*biezace));
        zadania[*biezace].q=tmp_q;
    }
}



size_t Projekt::r_p_poprzedniego_tech(size_t id)
{
    if (zadania[id].poprz_tech==SIZE_MAX)
        return 0;
    return zadania[zadania[id].poprz_tech].r + zadania[zadania[id].poprz_tech].p;
}

size_t Projekt::q_p_nastepnego_tech(size_t id)
{
    if (zadania[id].nast_tech==SIZE_MAX)
        return 0;
    return zadania[zadania[id].nast_tech].q + zadania[zadania[id].nast_tech].p;
}
size_t Projekt::r_p_poprzedniego_kol(size_t id)
{
    if (zadania[id].poprz_kol==SIZE_MAX)
        return 0;
    return zadania[zadania[id].poprz_kol].r + zadania[zadania[id].poprz_kol].p;
}

size_t Projekt::q_p_nastepnego_kol(size_t id)
{
    if (zadania[id].nast_kol==SIZE_MAX)
        return 0;
    return zadania[zadania[id].nast_kol].q + zadania[zadania[id].nast_kol].p;
}

size_t Projekt::m_zadania(size_t id)
{
    return zadania[id].m;
}

size_t Projekt::p_zadania(size_t id)
{
    return zadania[id].p;
}

size_t Projekt::r_p_zadania(size_t id)
{
    return zadania[id].r + zadania[id].p;
}

size_t Projekt::q_p_zadania(size_t id)
{
    return zadania[id].q + zadania[id].p;
}


void Projekt::insa()
{
    vector<Zadanie> zadania_sort(zadania.begin(),zadania.end());
    vector<size_t> kolejnosc;
    kolejnosc.reserve(zadania_sort.size());
    stable_sort(zadania_sort.rbegin(),zadania_sort.rend(),compare_zadanie());
//  sort(zadania_sort.begin(),zadania_sort.end());

    for(auto & zadanie: zadania_sort)
        kolejnosc.push_back(zadanie.id);

    vector< list< size_t > > zadania_insa;
    zadania_insa.resize(maszyn);

    for(auto & biezace: kolejnosc) //przejscie zadan posorotwanych od najwiekszego p
    {
        size_t min_c = SIZE_MAX;

        list<size_t> & biezaca_lista = zadania_insa[zadania[biezace].m];

        auto gdzie_wstawic = biezaca_lista.begin();
        if (!biezaca_lista.empty())
        {
            //wstawianie przed pierwszym
            size_t biezace_max_r=0;
            size_t biezace_max_q=max(q_p_nastepnego_tech(biezace), q_p_zadania(biezaca_lista.front()) );
            min_c = r_p_zadania(biezace) + biezace_max_q;
            //gdzie wstawic = biezaca_lista.begin() (juz ustwaione)

            size_t biezace_c;
            //wstawianie "nie na brzegach", przed 'pozycja'
            if(biezaca_lista.size()>1) //wstawianie w pozostalych miejscach
                for(auto pozycja=next(biezaca_lista.begin()); pozycja!=biezaca_lista.end(); pozycja++) //przejscie listy ulozonych elementow na maszynie biezacego zadania
                {
                    biezace_max_r=max(r_p_poprzedniego_tech(biezace), r_p_zadania(*prev(pozycja)));
                    biezace_max_q=max(q_p_nastepnego_tech(biezace), q_p_zadania(*pozycja));
                    biezace_c = biezace_max_r + p_zadania(biezace) + biezace_max_q;
                    if (biezace_c < min_c)
                    {
                        gdzie_wstawic = pozycja;
                        min_c = biezace_c;
                    }
                }
            //wstawianie za ostatnim
            biezace_max_r = max(r_p_poprzedniego_tech(biezace), r_p_zadania(*prev(biezaca_lista.end())) );
            biezace_c = biezace_max_r + q_p_zadania(biezace);
            if (biezace_c < min_c)
            {
                gdzie_wstawic=biezaca_lista.end();
                min_c = biezace_c;
            }
        }

        //wstawianie zadania
        auto wstawione_zadanie = biezaca_lista.insert(gdzie_wstawic, biezace);

        //aktualizowanie poprzednikow/nastepnikow
        if(wstawione_zadanie!=biezaca_lista.begin()) // jezeli nie wstawione na poczatku
        {
            //jezeli wstawione zadanie nie mialo poprzednikow
            if(zadania[*wstawione_zadanie].poprz_kol==SIZE_MAX) zadania[*wstawione_zadanie].n_poprzednikow++;
            zadania[*wstawione_zadanie].poprz_kol=*prev(wstawione_zadanie);
            //
            zadania[*prev(wstawione_zadanie)].nast_kol=*wstawione_zadanie;
        }
        auto ostatni = prev(biezaca_lista.end());
        if(wstawione_zadanie!=ostatni) // jezeli nie wstawiono na koncu
        {
            zadania[*wstawione_zadanie].nast_kol=*next(wstawione_zadanie);
            //jezeli nastepny nie mial wczesniej poprzednika
            if (zadania[*next(wstawione_zadanie)].poprz_kol==SIZE_MAX) zadania[*next(wstawione_zadanie)].n_poprzednikow++;
            zadania[*next(wstawione_zadanie)].poprz_kol=*wstawione_zadanie;
        }

        sortuj_topologicznie();
        licz_r_q();

    }

    vector<size_t> c_maszyn;
    for (auto & grupa: zadania_insa) {
        c_maszyn.push_back(zadania[grupa.front()].q + zadania[grupa.front()].p);
        //        for (auto & id_zadania: grupa)
        //            cout << id_zadania+1 << " ";
        //        cout << endl;
    }
    //    cout<<*max_element(c_maszyn.begin(), c_maszyn.end());
    if (cmax==*max_element(c_maszyn.begin(), c_maszyn.end()))
        cout<<"wynik zgodny" << endl;
    else
        cout << "roznica w wynikach (ujemny - uzyskano gorszy niz przykladowy) " << (int)(cmax - *max_element(c_maszyn.begin(), c_maszyn.end()))  << endl;

}


int Projekt::wczytaj(string nazwa_pliku,string data_id)
{
    ifstream plik;
    plik.open(nazwa_pliku.c_str());
    if (plik.is_open())
    {
        string linia;
        while(linia.find(data_id+":")==string::npos)
        {
            getline(plik, linia);
        }
        plik >> zadan;
        plik >> maszyn;
        plik >> operacji;
        for(size_t i=0;i<zadan;i++)
        {
            size_t operacji_w_zadaniu;
            plik >> operacji_w_zadaniu;
            for(size_t j=0;j<operacji_w_zadaniu;j++)
            {
                size_t m, p;
                plik >> m;
                m--;
                plik >> p;
                Zadanie zadanie(i*maszyn+j,p,m);
                size_t poprz_tech=SIZE_MAX;
                size_t nast_tech=SIZE_MAX;
                if(j!=0)
                {
                    poprz_tech=i*maszyn+j-1;
                    zadanie.n_poprzednikow=1;
                }
                if(j!=operacji_w_zadaniu-1)
                    nast_tech=i*maszyn+j+1;
                zadanie.poprz_tech=poprz_tech;
                zadanie.nast_tech=nast_tech;
                zadania.push_back(zadanie);
            }
        }
        while(linia.find("insa:")==string::npos)
        {
            getline(plik, linia);
        }
        plik>>cmax;
    }
    return 0;
}



int main()
{

    vector<string> dane;
    for(size_t i=0;i<=80;i++) //3717
    {
        stringstream liczba;
        liczba << '0'<< i/10 << i%10;
        dane.push_back(liczba.str());
    }
    clock_t t = clock();
    for(auto & liczba: dane)
    {
        cout << endl << liczba << endl;
        Projekt projekt;
        projekt.wczytaj("data.txt",liczba);
        //clock_t t = clock();
        projekt.sortuj_topologicznie();
        projekt.licz_r_q();
        projekt.insa();


    }
    t = clock() - t;
    cout << "czas: " << (double)t/CLOCKS_PER_SEC << endl;
}

